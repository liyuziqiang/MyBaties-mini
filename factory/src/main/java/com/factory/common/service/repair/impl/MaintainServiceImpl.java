package com.factory.common.service.repair.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.factory.common.mapper.SysUserMapper;
import com.factory.common.mapper.repair.MaintainMapper;
import com.factory.common.mapper.repair.WorkListRepairMapper;
import com.factory.common.model.Maintain;
import com.factory.common.model.PageQuery;
import com.factory.common.model.SysDept;
import com.factory.common.model.WorkListRepair;
import com.factory.common.service.repair.MaintainService;
import com.factory.common.service.repair.WorkListRepairService;
import com.factory.common.utils.ModelHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class MaintainServiceImpl implements MaintainService {

	@Autowired
	private MaintainMapper maintainMapper;

	public PageInfo<Map<String, Object>> findAll(
			Map<String, Object> queryMap) {
		 // 格式化map
        PageQuery query = new PageQuery(queryMap, WorkListRepair.class);
        // 分页条件
        PageHelper.startPage(query.getPage(), query.getLimit());
        long startT = System.currentTimeMillis();
		List<Maintain> repairList = maintainMapper.findAll(query.getData());
		long endT = System.currentTimeMillis();
		Long workRepairCount= maintainMapper.selectAmount(query.getData());
		 PageInfo<Map<String, Object>> repairPageInfo = null;
		 if(repairList!=null && repairList.size() > 0 ){
			 try {
	                // 先转json，再转回map
	                ObjectMapper mapper = new ObjectMapper();
	                String json = mapper.writeValueAsString(repairList);
	                List<Map<String, Object>> mapList = mapper.readValue(json, List.class);
	                repairPageInfo = new PageInfo<Map<String, Object>>(mapList);
	                repairPageInfo.setTotal(workRepairCount);
	            } catch (JsonProcessingException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
		 }
		return repairPageInfo;
	}


	public Maintain selectByPrimaryKey(String repairId) {
		return maintainMapper.selectByPrimaryKey(repairId);
	}
	public int create(Maintain maintain) {
		return maintainMapper.create(maintain);
	}

	public int update(Maintain maintain) {//c98757f9b7134ad28d93ef1821ba2d52
		Maintain main = new Maintain();
		main.setMaintainId("c98757f9b7134ad28d93ef1821ba2d52");
		main.setMaintainPerson("黎明");
		main.setCheckPerson("刘德华");
		//main.setDeviceId(UUID.randomUUID().toString().replace("-", ""));
		main.setMaintainStatus(2);
		main.setPlanEndTime(new Date());
		main.setPlanStartTime(new Date());
		main.setRealEndTime(new Date());
		return maintainMapper.update(maintain);
	}

	public int delete(String maintainId) {
		
		return maintainMapper.delete(maintainId);

	}
	
	public static void main(String[] args) {
		
		
		
	}


	

}
