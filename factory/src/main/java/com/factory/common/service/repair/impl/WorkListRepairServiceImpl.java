package com.factory.common.service.repair.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.factory.common.mapper.SysUserMapper;
import com.factory.common.mapper.repair.WorkListRepairMapper;
import com.factory.common.model.PageQuery;
import com.factory.common.model.SysDept;
import com.factory.common.model.WorkListRepair;
import com.factory.common.service.repair.WorkListRepairService;
import com.factory.common.utils.ModelHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class WorkListRepairServiceImpl implements WorkListRepairService {

	@Autowired
	private WorkListRepairMapper repairMapper;

	public PageInfo<Map<String, Object>> findAll(
			Map<String, Object> queryMap) {
		 // 格式化map
        PageQuery query = new PageQuery(queryMap, WorkListRepair.class);
        // 分页条件
        PageHelper.startPage(query.getPage(), query.getLimit());
        long startT = System.currentTimeMillis();
		List<WorkListRepair> repairList = repairMapper.findAll(query.getData());
		long endT = System.currentTimeMillis();
		Long workRepairCount= repairMapper.selectAmount(query.getData());
		 PageInfo<Map<String, Object>> repairPageInfo = null;
		 if(repairList!=null && repairList.size() > 0 ){
			 try {
	                // 先转json，再转回map
	                ObjectMapper mapper = new ObjectMapper();
	                String json = mapper.writeValueAsString(repairList);
	                List<Map<String, Object>> mapList = mapper.readValue(json, List.class);
	                repairPageInfo = new PageInfo<Map<String, Object>>(mapList);
	                repairPageInfo.setTotal(workRepairCount);
	            } catch (JsonProcessingException e) {
	                e.printStackTrace();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
		 }
		return repairPageInfo;
	}


	public WorkListRepair selectByPrimaryKey(String repairId) {
		return repairMapper.selectByPrimaryKey(repairId);
	}
	public int create(WorkListRepair repair) {
		return repairMapper.create(repair);
	}

	public int updateWorkRepair(WorkListRepair repair) {//c2a446dcb963457292c371123ca5207d
		return repairMapper.updateWorkRepair(repair);
	}

	public int deleteWorklistByKey(String repair_id) {
		repairMapper.deleteWorklistByKey(repair_id);
		return 0;

	}
	
	public static void main(String[] args) {
		
		
		
	}

}
