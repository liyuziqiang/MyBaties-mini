package com.factory.common.service.repair;

import java.util.List;
import java.util.Map;

import com.factory.common.model.WorkListRepair;
import com.github.pagehelper.PageInfo;

public interface WorkListRepairService {
		//查所有
		PageInfo<Map<String, Object>> findAll(Map<String, Object> queryMap);
		//查数量
		// Long selectAmount(Map<String,Object> data);
		 //根据主键查
		 WorkListRepair selectByPrimaryKey(String repairId);
		 //插入
		 int create(WorkListRepair repair);
		//修改
		int updateWorkRepair(WorkListRepair repair);
		//删除
		int deleteWorklistByKey(String repair_id);
}
