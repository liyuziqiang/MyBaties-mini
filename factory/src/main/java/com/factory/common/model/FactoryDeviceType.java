package com.factory.common.model;
/**
 * 设备类型
 * @author dell
 *
 */
public class FactoryDeviceType {
   
	private String typeId;
	private String typeCode;
	private String typeName;
	private Byte typeIsDelete;
	
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Byte getTypeIsDelete() {
		return typeIsDelete;
	}
	public void setTypeIsDelete(Byte typeIsDelete) {
		this.typeIsDelete = typeIsDelete;
	}
	
}
