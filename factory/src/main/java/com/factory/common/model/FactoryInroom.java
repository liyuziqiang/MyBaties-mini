package com.factory.common.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 入库管理
 * @author Administrator
 *
 */
public class FactoryInroom {

	private String inroomId;
	private String productName;
	private String productType;
	private String productStandard;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date inroomTime;
	private String inroomUser;
	private Long inroomNumber;
	private String factoryName;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date inBuyTime;
	private String inLayplace;
	private String inSupplier;
	private String inSupplierName;
	private String inSupplierPhone;
	private String editUser;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date editTime;
	private String inNote;
	
	
	public String getInroomId() {
		return inroomId;
	}
	public void setInroomId(String inroomId) {
		this.inroomId = inroomId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductStandard() {
		return productStandard;
	}
	public void setProductStandard(String productStandard) {
		this.productStandard = productStandard;
	}
	public Date getInroomTime() {
		return inroomTime;
	}
	public void setInroomTime(Date inroomTime) {
		this.inroomTime = inroomTime;
	}
	public String getInroomUser() {
		return inroomUser;
	}
	public void setInroomUser(String inroomUser) {
		this.inroomUser = inroomUser;
	}
	public Long getInroomNumber() {
		return inroomNumber;
	}
	public void setInroomNumber(Long inroomNumber) {
		this.inroomNumber = inroomNumber;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public Date getInBuyTime() {
		return inBuyTime;
	}
	public void setInBuyTime(Date inBuyTime) {
		this.inBuyTime = inBuyTime;
	}
	public String getInLayplace() {
		return inLayplace;
	}
	public void setInLayplace(String inLayplace) {
		this.inLayplace = inLayplace;
	}
	public String getInSupplier() {
		return inSupplier;
	}
	public void setInSupplier(String inSupplier) {
		this.inSupplier = inSupplier;
	}
	public String getInSupplierName() {
		return inSupplierName;
	}
	public void setInSupplierName(String inSupplierName) {
		this.inSupplierName = inSupplierName;
	}
	public String getInSupplierPhone() {
		return inSupplierPhone;
	}
	public void setInSupplierPhone(String inSupplierPhone) {
		this.inSupplierPhone = inSupplierPhone;
	}
	public String getEditUser() {
		return editUser;
	}
	public void setEditUser(String editUser) {
		this.editUser = editUser;
	}
	public Date getEditTime() {
		return editTime;
	}
	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}
	public String getInNote() {
		return inNote;
	}
	public void setInNote(String inNote) {
		this.inNote = inNote;
	}
	
}
