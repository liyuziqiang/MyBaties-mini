package com.factory.common.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 例行维护
 * 
 * @author Administrator
 *
 */
public class Maintain {
	private String maintainId;
	private String deviceName;
	private String maintainPerson;
	private Integer maintainStatus;
	private Date planStartTime;
	private Date planEndTime;
	private Date realEndTime;
	private String checkPerson;
	private Integer miantainFrequency;
	public Maintain(){}
	public String getMaintainId() {
		return maintainId;
	}
	public void setMaintainId(String maintainId) {
		this.maintainId = maintainId;
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getMaintainPerson() {
		return maintainPerson;
	}
	public void setMaintainPerson(String maintainPerson) {
		this.maintainPerson = maintainPerson;
	}
	public Integer getMaintainStatus() {
		return maintainStatus;
	}
	public void setMaintainStatus(Integer maintainStatus) {
		this.maintainStatus = maintainStatus;
	}
	public Date getPlanStartTime() {
		return planStartTime;
	}
	public void setPlanStartTime(Date planStartTime) {
		this.planStartTime = planStartTime;
	}
	public Date getPlanEndTime() {
		return planEndTime;
	}
	public void setPlanEndTime(Date planEndTime) {
		this.planEndTime = planEndTime;
	}
	
	public Date getRealEndTime() {
		return realEndTime;
	}
	public void setRealEndTime(Date realEndTime) {
		this.realEndTime = realEndTime;
	}
	public String getCheckPerson() {
		return checkPerson;
	}
	public void setCheckPerson(String checkPerson) {
		this.checkPerson = checkPerson;
	}
	public Integer getMiantainFrequency() {
		return miantainFrequency;
	}
	public void setMiantainFrequency(Integer miantainFrequency) {
		this.miantainFrequency = miantainFrequency;
	}

}
