package com.factory.common.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 任务巡检详细数据
 * @author dell
 *
 */
public class TaskAssignDetail {
    private String dDetailId;  
	private String aAssignId;   //任务分配ID
	private String tDetailId;   //任务详细ID
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dOverTime;
	
	private Byte dStatus;
	private String dPlaceParameter;
	private String faultCode;
	private String dNote;
	public String getdDetailId() {
		return dDetailId;
	}
	public void setdDetailId(String dDetailId) {
		this.dDetailId = dDetailId;
	}
	public String getaAssignId() {
		return aAssignId;
	}
	public void setaAssignId(String aAssignId) {
		this.aAssignId = aAssignId;
	}
	public String gettDetailId() {
		return tDetailId;
	}
	public void settDetailId(String tDetailId) {
		this.tDetailId = tDetailId;
	}
	public Date getdOverTime() {
		return dOverTime;
	}
	public void setdOverTime(Date dOverTime) {
		this.dOverTime = dOverTime;
	}
	public Byte getdStatus() {
		return dStatus;
	}
	public void setdStatus(Byte dStatus) {
		this.dStatus = dStatus;
	}
	public String getdPlaceParameter() {
		return dPlaceParameter;
	}
	public void setdPlaceParameter(String dPlaceParameter) {
		this.dPlaceParameter = dPlaceParameter;
	}
	public String getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	public String getdNote() {
		return dNote;
	}
	public void setdNote(String dNote) {
		this.dNote = dNote;
	}
	
}
