package com.factory.common.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * 设施设备
 * @author dell
 *
 */
public class FactoryDevice {
    private String id;
	private String deviceId;
	private String deviceName;
	private String labelId;
	private Byte status;
	private String faultType;
	
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date lastTime;
    
	private String placeParameter;
	private Byte isUse;
	private String deviceType;
	private String factoryName;
	private String outFactoryId;
	private String standardType;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date buyTime;
	
	private String layPlace;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date layTime;
	
	private String useYear;       //使用年限
	private String supplier;      //供应商
	private String supplierName;
	private String supplierPhone;
	private String devicePicture;
	private Byte isDelete;
	private String note;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getLabelId() {
		return labelId;
	}
	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public String getFaultType() {
		return faultType;
	}
	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}
	public Date getLastTime() {
		return lastTime;
	}
	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
	public String getPlaceParameter() {
		return placeParameter;
	}
	public void setPlaceParameter(String placeParameter) {
		this.placeParameter = placeParameter;
	}
	public Byte getIsUse() {
		return isUse;
	}
	public void setIsUse(Byte isUse) {
		this.isUse = isUse;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getFactoryName() {
		return factoryName;
	}
	public void setFactoryName(String factoryName) {
		this.factoryName = factoryName;
	}
	public String getOutFactoryId() {
		return outFactoryId;
	}
	public void setOutFactoryId(String outFactoryId) {
		this.outFactoryId = outFactoryId;
	}
	public String getStandardType() {
		return standardType;
	}
	public void setStandardType(String standardType) {
		this.standardType = standardType;
	}
	public Date getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}
	public String getLayPlace() {
		return layPlace;
	}
	public void setLayPlace(String layPlace) {
		this.layPlace = layPlace;
	}
	public Date getLayTime() {
		return layTime;
	}
	public void setLayTime(Date layTime) {
		this.layTime = layTime;
	}
	public String getUseYear() {
		return useYear;
	}
	public void setUseYear(String useYear) {
		this.useYear = useYear;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierPhone() {
		return supplierPhone;
	}
	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}
	public String getDevicePicture() {
		return devicePicture;
	}
	public void setDevicePicture(String devicePicture) {
		this.devicePicture = devicePicture;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Byte getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Byte isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
