package com.factory.common.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 任务计划
 * @author dell
 *
 */
public class FactoryTask {
    private String tTaskId;
    private String tTaskName;
    private String tUserId;
    private String tContent;
    private String tFrequency;
    private String tDeviceNum;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tStartTime;
   
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tEndTime;
   
    private String tAddUSer;
   
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tAddTime;
   
    private String tEditUser;
   
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tEditTime;
   
    private Byte tIsdelete;
    private String tNote;
   
	public String gettTaskId() {
		return tTaskId;
	}
	public void settTaskId(String tTaskId) {
		this.tTaskId = tTaskId;
	}
	public String gettTaskName() {
		return tTaskName;
	}
	public void settTaskName(String tTaskName) {
		this.tTaskName = tTaskName;
	}
	public String gettUserId() {
		return tUserId;
	}
	public void settUserId(String tUserId) {
		this.tUserId = tUserId;
	}
	public String gettContent() {
		return tContent;
	}
	public void settContent(String tContent) {
		this.tContent = tContent;
	}
	public String gettFrequency() {
		return tFrequency;
	}
	public void settFrequency(String tFrequency) {
		this.tFrequency = tFrequency;
	}
	public String gettDeviceNum() {
		return tDeviceNum;
	}
	public void settDeviceNum(String tDeviceNum) {
		this.tDeviceNum = tDeviceNum;
	}
	public Date gettStartTime() {
		return tStartTime;
	}
	public void settStartTime(Date tStartTime) {
		this.tStartTime = tStartTime;
	}
	public Date gettEndTime() {
		return tEndTime;
	}
	public void settEndTime(Date tEndTime) {
		this.tEndTime = tEndTime;
	}
	public String gettAddUSer() {
		return tAddUSer;
	}
	public void settAddUSer(String tAddUSer) {
		this.tAddUSer = tAddUSer;
	}
	public Date gettAddTime() {
		return tAddTime;
	}
	public void settAddTime(Date tAddTime) {
		this.tAddTime = tAddTime;
	}
	public String gettEditUser() {
		return tEditUser;
	}
	public void settEditUser(String tEditUser) {
		this.tEditUser = tEditUser;
	}
	public Date gettEditTime() {
		return tEditTime;
	}
	public void settEditTime(Date tEditTime) {
		this.tEditTime = tEditTime;
	}
	public Byte gettIsdelete() {
		return tIsdelete;
	}
	public void settIsdelete(Byte tIsdelete) {
		this.tIsdelete = tIsdelete;
	}
	public String gettNote() {
		return tNote;
	}
	public void settNote(String tNote) {
		this.tNote = tNote;
	}
	   
}
