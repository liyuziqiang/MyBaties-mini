package com.factory.common.model;
/**
 * 任务详情表
 * @author dell
 *
 */
public class TaskDetail {
    private String tDetailId; 
	private String tTaskid;    //任务ID
	private String fId;        //设备ID
	private Byte tOrder;     //排序号
	
	public String gettDetailId() {
		return tDetailId;
	}
	public void settDetailId(String tDetailId) {
		this.tDetailId = tDetailId;
	}
	public String gettTaskid() {
		return tTaskid;
	}
	public void settTaskid(String tTaskid) {
		this.tTaskid = tTaskid;
	}
	public String getfId() {
		return fId;
	}
	public void setfId(String fId) {
		this.fId = fId;
	}
	public Byte gettOrder() {
		return tOrder;
	}
	public void settOrder(Byte tOrder) {
		this.tOrder = tOrder;
	}

	
}
