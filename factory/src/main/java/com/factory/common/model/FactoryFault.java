package com.factory.common.model;
/**
 * 故障类型
 * @author dell
 *
 */
public class FactoryFault {
	private String faultId;
	private String faultCode;
	private String faultName;
	private Byte faultIsDelete;
	
	public String getFaultId() {
		return faultId;
	}
	public void setFaultId(String faultId) {
		this.faultId = faultId;
	}
	public String getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	public String getFaultName() {
		return faultName;
	}
	public void setFaultName(String faultName) {
		this.faultName = faultName;
	}
	public Byte getFaultIsDelete() {
		return faultIsDelete;
	}
	public void setFaultIsDelete(Byte faultIsDelete) {
		this.faultIsDelete = faultIsDelete;
	}
   
   
}
