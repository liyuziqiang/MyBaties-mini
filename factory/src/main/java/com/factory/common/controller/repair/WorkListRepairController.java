package com.factory.common.controller.repair;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.factory.common.model.WorkListRepair;
import com.factory.common.service.repair.WorkListRepairService;
import com.factory.common.utils.ReturnHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="common/repair")
public class WorkListRepairController  {
	
	@Autowired
	WorkListRepairService repairService;

	 @RequestMapping(value = "selectWorkListRepair", method = {RequestMethod.GET})
	 @ResponseBody
	public Map<String,Object> selectWorkListRepair(@RequestParam String query) throws IOException, JsonMappingException, IOException{
		// TODO Auto-generated method stub
	    ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.readValue(query, Map.class);
		return  ReturnHelper.getDataMap(200, "success", repairService.findAll(map));
	}
	 @RequestMapping(value = "create", method = {RequestMethod.POST})
	 @ResponseBody
	public Map<String,Object> create(WorkListRepair repair) {
		  if (repairService.create(repair)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	 @RequestMapping(value = "updateWorkRepair", method = {RequestMethod.POST})
	 @ResponseBody
	public Map<String,Object> updateWorkRepair(WorkListRepair repair) {
		  if (repairService.updateWorkRepair(repair)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	 @RequestMapping(value = "deleteWorklistByKey", method = {RequestMethod.GET})
	 @ResponseBody
	public Map<String,Object> deleteWorklistByKey(@RequestParam String repair_id) {
		// TODO Auto-generated method stub
		 if (repairService.deleteWorklistByKey(repair_id)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	
	
}
