package com.factory.common.controller.repair;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.factory.common.model.Maintain;
import com.factory.common.model.WorkListRepair;
import com.factory.common.service.repair.MaintainService;
import com.factory.common.service.repair.WorkListRepairService;
import com.factory.common.utils.ReturnHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value="common/miantain")
public class MaintainController  {
	
	@Autowired
	MaintainService maintainService;

	 @RequestMapping(value = "selectMaintainRepair", method = {RequestMethod.GET})
	 @ResponseBody
	public Map<String,Object> selectMaintainRepair(@RequestParam String query) throws IOException, JsonMappingException, IOException{
		// TODO Auto-generated method stub
	    ObjectMapper mapper = new ObjectMapper();
	    if(query==null){
	    	
	    }Map<String, Object> map = mapper.readValue(query, Map.class);
		return  ReturnHelper.getDataMap(200, "success", maintainService.findAll(map));
	}
	 @RequestMapping(value = "create", method = {RequestMethod.POST})
	 @ResponseBody
	public Map<String,Object> create(Maintain maintain) {
		  if (maintainService.create(maintain)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	 @RequestMapping(value = "updateMaintainRepair", method = {RequestMethod.POST})
	 @ResponseBody
	public Map<String,Object> updateMaintainRepair(Maintain maintain) {
		  if (maintainService.update(maintain)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	 @RequestMapping(value = "deleteMaintainByKey", method = {RequestMethod.GET})
	 @ResponseBody
	public Map<String,Object> deleteMaintainByKey(@RequestParam String maintainId) {
		// TODO Auto-generated method stub
		 if (maintainService.delete(maintainId)== 1 ){
	            return ReturnHelper.getSimpleMap(200, "修改成功");
	        } else {
	            return ReturnHelper.getSimpleMap(500, "修改失败");
	        }
	}
	
	
}
