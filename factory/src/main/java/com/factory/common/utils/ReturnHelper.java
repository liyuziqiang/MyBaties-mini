package com.factory.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 返回map工具
 */
public class ReturnHelper {
    /**
     * 返回一个简单map
     * @param status 状态码
     * @param message  信息
     * @return map
     */
    public static Map<String, Object> getSimpleMap(int status, String message) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("status", status);
        map.put("message", message);
        return map;
    }

    /**
     * 返回一个带数据的map
     * @param status 状态码
     * @param message  信息
     * @param data  数据
     * @return map
     */
    public static Map<String, Object> getDataMap(int status, String message, Object data){
        Map<String, Object> map = getSimpleMap(status, message);
        map.put("data", data);
        return map;
    }
    
    /**
     * 表单验证错误时，返回表单验证错误信息
     * @param result 验证结果
     * @return map的json
     * @throws JsonProcessingException json转化错误
     */
    public static Map<String, Object> getVaildError(BindingResult result) throws JsonProcessingException {
        Map<String, Object> map = getSimpleMap(500, "输入表单信息错误");
        List<FieldError> errors = result.getFieldErrors();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (FieldError error: errors) {
            Map<String, Object> errorMap = new HashMap<String, Object>();
            errorMap.put("errorMessage", error.getDefaultMessage());
            errorMap.put("field", error.getField());
            list.add(errorMap);
        }
        map.put("data", list);
        return map;
    }
}
