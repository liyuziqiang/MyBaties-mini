package com.factory.common.utils;

import com.factory.common.model.SysResource;
import com.factory.common.model.SysUser;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 实体类工具
 */
public class ModelHelper {

    /**
     * 获取添加信息，（添加信息用户和添加时间）
     *
     * @param o 需要添加信息的对象
     */
    public static void getAddInfo(Object o) {
        getInfo(o, "setAddUser", "setAddTime");
    }

    /**
     * 获取修改信息，（修改信息用户和修改时间）
     *
     * @param o
     */
    public static void getEditInfo(Object o) {
        getInfo(o, "setEditUser", "setEditTime");
    }

    private static void getInfo(Object o, String setAddUser, String setAddTime) {
        Method[] methods = o.getClass().getDeclaredMethods();
        try {
            for (Method m : methods) {
                if (m.getName().contains(setAddUser)) {
                    m.invoke(o, getUser().getUserId());
                } else if (m.getName().contains(setAddTime)) {
                    m.invoke(o, DateHelper.getSysTimestamp());
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前session中的用户
     */
    public static SysUser getUser() {
        Session session = SecurityUtils.getSubject().getSession();
        if (session != null) {
            if (session != null) {
                return (SysUser) session.getAttribute("userInfo");
            }
        }
        return null;
    }

    /**
     * 获取一个UUID
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 判断获取查询结果是否空，若为空返回null，否则返回第一个值
     */
    public static <T> T getFirst(List<T> list) {
        if (list == null && list.size() == 0) {
            return null;
        }
        return list.get(0);
    }
    
    /**
     * 判断输入的是否包含中文，若是返回空
     */
    public static String isHaveChinese(String infoMessage){
		Matcher m = Pattern.compile("[\u4e00-\u9fa5]").matcher(infoMessage);
        if(m.find()){
    		return null;
        }
		return infoMessage;
    }
}
