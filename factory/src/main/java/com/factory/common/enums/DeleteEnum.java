package com.factory.common.enums;

import java.util.HashMap;
import java.util.Map;

public enum DeleteEnum {
    delete(1, "删除"),
    noDelete(0, "未删除");


    private int code;
    private String displayName;
    static Map<Integer, DeleteEnum> enumMap = new HashMap<Integer, DeleteEnum>();
    static{
        for(DeleteEnum type: DeleteEnum.values()){
            enumMap.put(type.getCode(), type);
        }
    }

    DeleteEnum(int code, String displayName) {
        this.code = code;
        this.displayName = displayName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    // 通过code获取enum
    public static DeleteEnum getEnum(int value) {
        return enumMap.get(value);
    }
}
