package com.factory.common.mapper.taskMapper;

import java.util.List;
import java.util.Map;

import com.factory.common.model.FactoryTask;
import com.factory.common.model.FactoryTaskDetail;

public interface TaskDetailMapper {

	int insertTaskDetail(FactoryTaskDetail taskDetail);

//	List<FactoryTaskDetail> findAllTaskDetail(Map<String, Object> data);

	Long findAllCount(Map<String, Object> data);

	int deleteByTaskDetailId(String id);

	void deleteByTaskId(String gettTaskId);

//	int updateByTaskDetailId(FactoryTaskDetail taskDetail);

}
