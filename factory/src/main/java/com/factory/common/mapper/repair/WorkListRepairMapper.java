package com.factory.common.mapper.repair;

import java.util.List;
import java.util.Map;

import com.factory.common.model.FactoryFault;
import com.factory.common.model.WorkListRepair;
import com.github.pagehelper.PageInfo;

public interface WorkListRepairMapper {
	
	//查所有
	 List<WorkListRepair> findAll(Map<String, Object> data);
	//查数量
	 Long selectAmount(Map<String,Object> data);
	 //根据主键查
	 WorkListRepair selectByPrimaryKey(String repairId);
	 //插入
	 int create(WorkListRepair repair);
	//修改
	int updateWorkRepair(WorkListRepair repair);
	//删除
	void deleteWorklistByKey(String repair_id);
}
