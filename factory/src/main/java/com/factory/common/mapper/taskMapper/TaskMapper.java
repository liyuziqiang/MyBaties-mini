package com.factory.common.mapper.taskMapper;

import java.util.List;
import java.util.Map;

import com.factory.common.model.FactoryTask;

public interface TaskMapper {

	int insertSelective(FactoryTask task);

	List<FactoryTask> findAll(Map<String, Object> data);

	Long findAllCount(Map<String, Object> data);

	int deleteByPrimaryKey(String id);

	int updateByPrimaryKeySelective(FactoryTask task);

}
