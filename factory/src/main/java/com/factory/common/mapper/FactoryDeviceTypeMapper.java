package com.factory.common.mapper;

import com.factory.common.model.FactoryDevice;
import com.factory.common.model.FactoryDeviceType;
import com.factory.common.model.SysRole;
import com.factory.common.model.SysRoleExample;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface FactoryDeviceTypeMapper {

    int deleteByPrimaryKey(String typeId);

    int insertSelective(FactoryDeviceType deviceType);

    FactoryDevice selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(FactoryDeviceType deviceType);

    List<FactoryDeviceType> findAll(Map<String, Object> queryMap);

	String getDeviceTypeCountMax();

	Long findAllCount(Map<String, Object> data);

}