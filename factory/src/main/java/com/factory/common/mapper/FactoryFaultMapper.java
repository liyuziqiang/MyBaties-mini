package com.factory.common.mapper;

import com.factory.common.model.FactoryFault;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface FactoryFaultMapper {

    int deleteByPrimaryKey(String id);

    int insertSelective(FactoryFault deviceType);

    FactoryFault selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(FactoryFault deviceType);

    List<FactoryFault> findAll(Map<String, Object> queryMap);

	String getDeviceFaultCountMax();

	Long findAllCount(Map<String, Object> data);

}