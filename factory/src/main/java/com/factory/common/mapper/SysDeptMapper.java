package com.factory.common.mapper;

import com.factory.common.model.SysDept;
import com.factory.common.model.SysDeptExample;
import com.factory.common.model.SysRole;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SysDeptMapper {
    long countByExample(SysDeptExample example);

    int deleteByExample(SysDeptExample example);

    int deleteByPrimaryKey(String deptId);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    List<SysDept> selectByExample(SysDeptExample example);

    SysDept selectByPrimaryKey(String deptId);

    int updateByExampleSelective(@Param("record") SysDept record, @Param("example") SysDeptExample example);

    int updateByExample(@Param("record") SysDept record, @Param("example") SysDeptExample example);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);

	List<SysDept> findAll(Map<String, Object> data);

	List<Map<String, Object>> findAllDeptTree();

	Long findAllCount(Map<String, Object> data);
}