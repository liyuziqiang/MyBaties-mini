package com.factory.common.mapper.partsStorageMapper;

import java.util.List;
import java.util.Map;

import com.factory.common.model.FactoryInroom;

/**
 * 入库管理
 * @author Administrator
 *
 */
public interface FactoryInroomMapper {

	int insertSelective(FactoryInroom factoryInroom);

	List<FactoryInroom> findAll(Map<String, Object> data);

	Long findAllCount(Map<String, Object> data);

	int deleteByPrimaryKey(String id);

	int updateByPrimaryKeySelective(FactoryInroom factoryInroom);
}
