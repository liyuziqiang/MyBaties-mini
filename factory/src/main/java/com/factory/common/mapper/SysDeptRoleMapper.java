package com.factory.common.mapper;

import com.factory.common.model.SysDeptRole;
import com.factory.common.model.SysDeptRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysDeptRoleMapper {
    long countByExample(SysDeptRoleExample example);

    int deleteByExample(SysDeptRoleExample example);

    int deleteByPrimaryKey(String uuid);

    int insert(SysDeptRole record);

    int insertSelective(SysDeptRole record);

    List<SysDeptRole> selectByExample(SysDeptRoleExample example);

    SysDeptRole selectByPrimaryKey(String uuid);

    int updateByExampleSelective(@Param("record") SysDeptRole record, @Param("example") SysDeptRoleExample example);

    int updateByExample(@Param("record") SysDeptRole record, @Param("example") SysDeptRoleExample example);

    int updateByPrimaryKeySelective(SysDeptRole record);

    int updateByPrimaryKey(SysDeptRole record);
}