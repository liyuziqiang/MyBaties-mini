package com.factory.common.mapper;

import com.factory.common.model.SysResource;
import com.factory.common.model.SysResourceExample;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface SysResourceMapper {
    long countByExample(SysResourceExample example);

    int deleteByExample(SysResourceExample example);

    int deleteByPrimaryKey(String resourceId);

    int insert(SysResource record);

    int insertSelective(SysResource record);

    List<SysResource> selectByExample(SysResourceExample example);

    SysResource selectByPrimaryKey(String resourceId);

    int updateByExampleSelective(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    int updateByExample(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    int updateByPrimaryKeySelective(SysResource record);

    int updateByPrimaryKey(SysResource record);

    List<SysResource> findAll(Map<String, Object> query);

    List<Map<String, Object>> findByRole(String roleId);
    
    List<SysResource> findAllResource(Map<String, Object> query);

	Long findAllResourceCount(Map<String, Object> data);
}