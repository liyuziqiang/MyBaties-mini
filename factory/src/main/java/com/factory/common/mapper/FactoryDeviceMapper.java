package com.factory.common.mapper;

import com.factory.common.model.FactoryDevice;
import com.factory.common.model.SysRole;
import com.factory.common.model.SysRoleExample;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface FactoryDeviceMapper {

    int deleteByPrimaryKey(String id);


    int insertSelective(FactoryDevice device);


    FactoryDevice selectByPrimaryKey(String id);


    int updateByPrimaryKeySelective(FactoryDevice device);


    List<FactoryDevice> findAll(Map<String, Object> queryMap);

	Long findAllCount(Map<String, Object> data);
     
	List<FactoryDevice> findDeviceByTaskId(Map<String, Object> map);

	Long findDeviceByTaskIdCount(Map<String, Object> data);

}