package com.factory.common.mapper.repair;

import java.util.List;
import java.util.Map;

import com.factory.common.model.FactoryFault;
import com.factory.common.model.Maintain;
import com.factory.common.model.WorkListRepair;
import com.github.pagehelper.PageInfo;

public interface MaintainMapper {
	
	//查所有
	 List<Maintain> findAll(Map<String, Object> data);
	//查数量
	 Long selectAmount(Map<String,Object> data);
	 //根据主键查
	 Maintain selectByPrimaryKey(String maintainId);
	 //插入
	 int create(Maintain maintain);
	//修改
	int update(Maintain maintain);
	//删除
	int delete(String maintainId);
}
