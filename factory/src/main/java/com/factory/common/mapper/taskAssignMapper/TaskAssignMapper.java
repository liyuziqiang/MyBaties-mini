package com.factory.common.mapper.taskAssignMapper;

import java.util.List;
import java.util.Map;

import com.factory.common.model.TaskAssign;

public interface TaskAssignMapper {

	int insertSelective(TaskAssign taskAssgin);

	List<TaskAssign> findAll(Map<String, Object> data);

	Long findAllCount(Map<String, Object> data);

	int deleteByPrimaryKey(String id);

	int updateByPrimaryKeySelective(TaskAssign taskAssign);

	TaskAssign selectByPrimaryKey(String assignId);

	List<TaskAssign> findTaskAssignByTaskId(String id);

	List<TaskAssign> findAllTaskAssignByTaskId(String taskId);

}
