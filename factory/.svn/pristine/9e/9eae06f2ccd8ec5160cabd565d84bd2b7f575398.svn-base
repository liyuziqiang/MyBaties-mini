package com.factory.common.controller;

import com.factory.common.enums.DeleteEnum;
import com.factory.common.model.SysUser;
import com.factory.common.service.UserService;
import com.factory.common.utils.ReturnHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping(value = "itom")
public class TestController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "测试接口1", notes = "测试接口1,权限menu1:delete")
    @RequestMapping(value = "test1", method = {RequestMethod.GET})
    @ResponseBody
    @RequiresPermissions("menu1:delete")
    public Map<String, Object> test1() {
        return ReturnHelper.getSimpleMap(200, "test1");
    }

    @ApiOperation(value = "测试接口2", notes = "测试接口2，权限menu3:create")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "testID", required = true, dataType = "String", paramType = "query")})
    @RequestMapping(value = "test2", method = {RequestMethod.GET})
    @ResponseBody
    @RequiresPermissions("menu3:create")
    public Map<String, Object> test2(@RequestParam String id) {
        return ReturnHelper.getSimpleMap(200, "test2" + id);
    }

    @ApiOperation(value = "测试接口3", notes = "测试接口3,权限menu1:create,用于测试请求为一个model对象")
    @ApiImplicitParams({@ApiImplicitParam(name = "user", value = "用户详细实体user", required = true, dataType = "SysUser")})
    @RequestMapping(value = "test3", method = {RequestMethod.POST})
    @ResponseBody
    @RequiresPermissions("menu1:create")
    public Map<String, Object> test3(@RequestBody @Valid SysUser user, BindingResult result) throws JsonProcessingException {
        if (result.hasErrors()) {
            return ReturnHelper.getVaildError(result);
        }
        if (userService.createUser(user) == 1) {
            return ReturnHelper.getSimpleMap(200, "添加成功");
        } else {
            return ReturnHelper.getSimpleMap(500, "添加失败");
        }
    }

    @ApiOperation(value = "测试接口4", notes = "测试接口4,类型或者状态枚举使用")
    @ApiImplicitParams({@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "String", paramType = "query")})
    @RequestMapping(value = "test4", method = {RequestMethod.GET})
    @ResponseBody
    @RequiresPermissions("menu1:create")
    public Map<String, Object> test4(@RequestParam String userId) {
        SysUser sysUser = userService.findOne(userId);
        if (sysUser != null) {
            return ReturnHelper.getDataMap(200, "此用户状态", DeleteEnum.getEnum(sysUser.getIsDelete()).getDisplayName());
        } else {
            return ReturnHelper.getSimpleMap(500,"找不到用户");
        }
    }

    @ApiOperation(value = "测试接口5", notes = "测试接口5,分页功能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start", value = "分页开始位置,pageNum<=0 时会查询第一页， pageNum>pages（超过总数时），会查询最后一页。", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页的数量,如果 pageSize=0 或者 RowBounds.limit = 0 就会查询出全部的结果（相当于没有执行分页查询，但是返回结果仍然是 Page 类型）", required = true, dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "test5", method = {RequestMethod.GET})
    @ResponseBody
    @RequiresPermissions("menu1:create")
    public Map<String, Object> test5(@RequestParam int start, @RequestParam int limit) {
        return ReturnHelper.getDataMap(200, "分页数据", userService.findAllhavePage(start, limit));
    }
}
